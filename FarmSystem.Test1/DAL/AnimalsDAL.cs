﻿using FarmSystem.Test1.Interfaces;
using FarmSystem.Test1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmSystem.Test1.DAL
{
    public class AnimalsDAL : IAnimalsDAL
    {
        // pretend database
        private IDictionary<string, IAnimal> Animals = new Dictionary<string, IAnimal>()
        {
            { "Cow", new Cow() },
            { "Hen", new Hen() },
            { "Horse", new Horse()},
            { "Sheep", new Sheep() }

        };        

        public IDictionary<string,IAnimal> GetAnimals()
        {
            return Animals;
        }

        public IAnimal GetAnimal(string type)
        {
            if (Animals.ContainsKey(type))
            {
                return Animals[type];
            }

            throw new ArgumentException($"{type} does not exist in this animal kingdom!");

        }
    }
}
