﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmSystem.Test1.Interfaces
{
    public interface IAnimalsDAL
    {
        IDictionary<string, IAnimal> GetAnimals();
        IAnimal GetAnimal(string type);
    }
}
