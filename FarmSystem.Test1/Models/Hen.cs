﻿using FarmSystem.Test1.Interfaces;
using System;

namespace FarmSystem.Test1.Models
{
    public class Hen : BaseAnimal, IAnimal
    {
        public Hen() : base(2) { }

        public void Talk()
        {
            Console.WriteLine("Hen say CLUCKAAAAAWWWWK!");
        }

        public void Run()
        {
            Console.WriteLine("Hen is running");
        }
    }
}