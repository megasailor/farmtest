﻿using FarmSystem.Test1.Interfaces;
using System;

namespace FarmSystem.Test1.Models
{
    public class Horse : BaseAnimal, IAnimal
    {               
        public Horse() : base(4) { }

        public void Talk()
        {
            Console.WriteLine("Horse says neigh!");
        }

        public void Run()
        {
            Console.WriteLine("Horse is running");
        }
        
    }
}