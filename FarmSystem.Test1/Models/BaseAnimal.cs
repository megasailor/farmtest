﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmSystem.Test1.Models
{
    public abstract class BaseAnimal
    {
        public BaseAnimal(int noOfLegs)
        {
            _id = Guid.NewGuid().ToString();
            _noOfLegs = noOfLegs; 
        }

        protected string _id;
        protected int _noOfLegs;

        public string Id
        {
            get { return _id; }
            set
            {
                _id = value;
            }
        }

        public int NoOfLegs
        {
            get
            {
                return _noOfLegs;
            }
            set
            {
                _noOfLegs = value;
            }
        }

        public string GetTypeOfAnimal()
        {
            return this.GetType().Name;
        }
    }
}
