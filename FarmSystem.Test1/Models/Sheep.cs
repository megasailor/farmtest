﻿using FarmSystem.Test1.Interfaces;
using FarmSystem.Test2.Interfaces;
using System;

namespace FarmSystem.Test1.Models
{
    public class Sheep : BaseAnimal, IAnimal
    {
        public Sheep() : base(4) { }

        public void Talk()
        {
            Console.WriteLine("Sheep says baa!");
        }
        
        public void Run()
        {
            Console.WriteLine("Sheep is running");
        }
        
    }

}