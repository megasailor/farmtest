﻿using FarmSystem.Test1.Interfaces;
using FarmSystem.Test1.Models;
using FarmSystem.Test2.Interfaces;
using System;

namespace FarmSystem.Test1
{
    public class Cow : BaseAnimal, IAnimal, IMilkableAnimal
    {
        public Cow() : base(4)
        {
            
        }

        public void Talk()
        {
            Console.WriteLine("Cow says Moo!");
        }

        public void Walk()
        {
            Console.WriteLine("Cow is walking");
        }        

        public void Run()
        {
            Console.WriteLine("Cow is running");
        }

        public void ProduceMilk()
        {
            Console.WriteLine($"Cow was milked!");
        }
    }
}