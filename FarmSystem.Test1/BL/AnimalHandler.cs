﻿using FarmSystem.Test1.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmSystem.Test1.BL
{
    public class AnimalHandler
    {
        IAnimalsDAL _dal;
        public AnimalHandler(IAnimalsDAL dal)
        {
            _dal = dal;
        }

        public IAnimal GetAnimal(string type)
        {
            return _dal.GetAnimal(type);
        }

        public IDictionary<string, IAnimal> GetAnimals()
        {
            return _dal.GetAnimals();
        }

    }
}
