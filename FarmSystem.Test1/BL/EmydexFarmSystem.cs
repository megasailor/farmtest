﻿using FarmSystem.Test1.Interfaces;
using FarmSystem.Test2.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;

namespace FarmSystem.Test1
{
    public delegate void EventHandler();

    public class EmydexFarmSystem
    {
        private Queue StoredAnimals;
        public event EventHandler FarmEmpty;

        public EmydexFarmSystem()
        {
            if(StoredAnimals == null)
            {
                StoredAnimals = new Queue();
            }            
        }

        //TEST 1
        public void Enter(IAnimal animal)
        {
            //TODO Modify the code so that we can display the type of animal (cow, sheep etc) 
            //Hold all the animals so it is available for future activities
            StoredAnimals.Enqueue(animal);
            Console.WriteLine($"{animal.GetTypeOfAnimal()} has entered the Emydex farm");
        }
     
        //TEST 2
        public void MakeNoise()
        {
            //Test 2 : Modify this method to make the animals talk
            if(StoredAnimals.Count == 0)
            {
                Console.WriteLine("There are no animals in the farm");
            }
            foreach(IAnimal animal in StoredAnimals)
            {
                animal.Talk();
            }
           
        }

        //TEST 3
        public void MilkAnimals()
        {
            bool foundMilkers = false;   
            foreach(IAnimal animal in StoredAnimals)
            {
                
                if(animal is IMilkableAnimal)
                {
                    ((IMilkableAnimal)animal).ProduceMilk();
                    foundMilkers = true;
                }             
                
            }
            if (!foundMilkers)
            {
                Console.WriteLine("Cannot identify the farm animals which can be milked");

            }
        }

        //TEST 4
        public void ReleaseAllAnimals()
        {

            var animalCount = StoredAnimals.Count;
            for(var i = 0; i< animalCount; i++)
            {
                var animal = (IAnimal)StoredAnimals.Dequeue();
                Console.WriteLine($"{animal.GetTypeOfAnimal()} has left the farm");
            }

            

            if(StoredAnimals.Count > 0) { 
                Console.WriteLine("There are still animals in the farm, farm is not free");
            }
            else
            {
                FarmEmpty.Invoke();
            }
          
        }
    }
}